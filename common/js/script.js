/*
 * セルフリンク&スムーススクロール&ロールオーバー
 * グローバルナビのカレント表示
 * 縦アコーディオン
 * jquery.heightLine.js
 * スムーススクロール
 * "tel:" リンクを【スマートフォン端末以外では無効】
 * GoogleMap
*/

//テスト環境用
// $(function(){
// 	var path = location.pathname;
// 	if(path.indexOf('/test/') !== -1){
// 		$('#header-logo a, #breadcrumb li:first-child a, #header-nav-area li a.home').attr('href', '/test/');
// 	}
// });


//相対パスを絶対パスに変換
// function convertAbsUrl( src ){
//     return $("<a>").attr("href", src).get(0).href;
// }

//fatNav.js HBメニュー
$(document).ready(function () {
	$.fatNav();
});

//fatNav.js アニメーション

$(function () {
	$('#fade-in').on('inview', function (event, isInView) {
		if (isInView) {
			$(this).addClass('animated fadeIn');
		} else {
			//$(this).removeClass('animated fadeIn');
		}
	});
});



/*縦アコーディオン
 ---------------------------------------------------*/
$(function() {
    $('.accordion1 dt').click(function(){
        $(this).next().slideToggle('normal');
		$(this).children('span').toggleClass("open");
    });
});


/*スライダー
 ---------------------------------------------------*/
 //単体画像
$(function() {
	$('.single-item').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		speed: 2000,
		fade: true,
	});
});

//サムネイル付き
$(function() {
	$('.thumb-item').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.thumb-item-nav'
	});
	$('.thumb-item-nav').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.thumb-item',
		focusOnSelect: true,
		responsive: [{
			breakpoint: 668,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
			}
		},{
			breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
				}
			}
		]
	});
});


/* breakpoints.js
--------------------------------------------------------*/
$(function() {
    $(window).setBreakpoints({
        distinct: true,
        breakpoints: [ 1, 668 ]
    });
    $(window).bind('enterBreakpoint668',function() {
        $('.sp-img').each(function() {
            $(this).attr('src', $(this).attr('src').replace('_sp', '_pc'));
        });
    });
    $(window).bind('enterBreakpoint1',function() {
        $('.sp-img').each(function() {
            $(this).attr('src', $(this).attr('src').replace('_pc', '_sp'));
        });
    });
});


/* tel:リンクPCの時はオフ、SPの時はオン 
------------------------------------------------------*/
var ua = navigator.userAgent;
if (ua.indexOf('iPhone') < 0 && ua.indexOf('Android') < 0) {
 $('a[href^="tel:"]').css('cursor','default')
	.click(function (event) {
	 event.preventDefault(); 
	}); 
} 


/* jquery.heightLine.js
-------------------------------------------------------*/
$(window).load(function() {
$(".bottom-col>div").heightLine();
});


/* GoogleMAP拡大縮小ボタンの有効
-------------------------------------------------------*/
$('.gmap').click(function () {
$('.gmap iframe').css("pointer-events", "auto");
});


$(document).ready(function() {
    if(location.pathname != "/") {
        $('.pattern1-1-1 a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
    } else $('.pattern1-1-1 a:eq(0)').addClass('active');
});

// Globalnav current
$(function(){
var id = $("body").attr("id");
$(".pattern1-1-1 a .col.category."+id).addClass("current");
});

//スムーススクロール
// $(function(){
//    // #で始まるアンカーをクリックした場合に処理
//    $('a[href^=#]').click(function() {
//       // スクロールの速度
//       var speed = 400; // ミリ秒
//       // アンカーの値取得
//       var href= $(this).attr("href");
//       // 移動先を取得
//       var target = $(href == "#" || href == "" ? 'html' : href);
//       // 移動先を数値で取得
//       var position = target.offset().top;
//       // スムーススクロール
//       $('body,html').animate({scrollTop:position}, speed, 'swing');
//       return false;
//    });
// });

